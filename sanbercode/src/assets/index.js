const IMAGE_ICONS = {
    pokeball: require('./Pokeball.png'),
    pokeballBg: require('./Pokeball2.png'),
    pokeballWhite: require('./pokeball-white.png'),
    pokedex: require('./pokedex.png'),
}

export default IMAGE_ICONS;

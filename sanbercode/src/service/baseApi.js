import axios from "axios"

export const doGet = (url, params) => {
    const method = 'GET'
    const headers = {}
  
    return axios({url, method, headers, params})
}
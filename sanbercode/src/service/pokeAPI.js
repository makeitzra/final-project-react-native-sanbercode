import {Alert} from 'react-native';
import {doGet} from './baseApi';

export const getListPoke = async (limit, offset, dispatch) => {
  let result = {list: undefined, ok: false};
  const url = `https://pokeapi.co/api/v2/pokemon`;
  const params = {limit: 50};
  try {
    const response = await doGet(url, params);
    if (response) {
      result.ok = true;
      result.list = response.data.results;
    }
  } catch (err) {
    console.log(err);
    // Alert.alert(err.response)
  }
  return result;
};

export const getPokeDetail = async url => {
  let result = {detail: undefined, ok: false};
  const params = {};
  try {
    const response = await doGet(url, params);
    if (response) {
      result.ok = true;
      result.detail = response.data;
    }
  } catch (err) {
    console.log(err);
    // Alert.alert(err.response)
  }
  return result;
};

import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { COLOR_SCHEME } from "../ColorScheme/ColorScheme";

const CustomButton = ({ children, variant, onPress}) => {
  return (
    <TouchableOpacity
      style={
        variant === "primary"
          ? styles.appButtonContainer
          : styles.appButtonContainerSecondary
      }
      onPress={onPress}
    >
      <Text
        style={
          variant === "primary"
            ? styles.appButtonText
            : styles.appButtonTextSecondary
        }
      >
        {children}
      </Text>
    </TouchableOpacity>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  appButtonContainer: {
    elevation: 8,
    backgroundColor: COLOR_SCHEME.SECONDARY,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginVertical: 10,
    width:300,
  },
  appButtonContainerSecondary: {
    elevation: 8,
    backgroundColor: "white",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderColor: COLOR_SCHEME.PRIMARY,
    marginVertical: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase",
  },
  appButtonTextSecondary: {
    fontSize: 18,
    color: COLOR_SCHEME.SECONDARY,
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase",
  },
});

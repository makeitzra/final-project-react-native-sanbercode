import { StyleSheet, Text, TextInput, View } from "react-native";
import { COLOR_SCHEME } from "../ColorScheme/ColorScheme";

const TextFields = ({ label, placeholder, value, onChange }) => {
  return (
    <View style={styles.container}>
      <TextInput placeholder={placeholder} style={styles.input} onChangeText={onChange} value={value}/>
    </View>
  );
};

export default TextFields;

const styles = StyleSheet.create({
  container: {
    width: 300,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginTop: 15,
  },
  input: {
    width: '100%',
    height: 50,
    marginTop: 5,
    borderWidth: 1,
    borderColor: COLOR_SCHEME.PRIMARY,
    padding: 10,
    borderRadius: 10
  },
});

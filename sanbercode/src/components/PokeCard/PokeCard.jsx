import React from 'react';
import {Button, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {SvgUri} from 'react-native-svg';
import capitalizeFirstLetter from '../../helper/capitalizeFirstLetter';
import CustomButton from '../Button/CustomButton';
import {COLOR_SCHEME} from '../ColorScheme/ColorScheme';

const PokeCard = ({name, number, release }) => {
  return (
      <View style={styles.container}>
        <Text style={{height: '15%', fontSize: 10, marginRight: 5}}>
          #{number}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            height: '70%',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
          }}>
          <SvgUri
            width="100%"
            height="100%"
            style={{marginLeft: -180}}
            uri={`https://unpkg.com/pokeapi-sprites@2.0.2/sprites/pokemon/other/dream-world/${number}.svg`}
          />
          <TouchableOpacity
            style={{
              position: 'absolute',
              right: 20,
              backgroundColor: '#FA5C5C',
              padding: 7,
              borderRadius: 10,
            }}
            onPress={release}
            >
            <Text style={{color: 'white'}}>Release Your Pokemon</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: '15%',
            width: '100%',
            backgroundColor: COLOR_SCHEME.PRIMARY,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 12,
              fontWeight: '600',
              color: 'white',
            }}>
            {capitalizeFirstLetter(name)}
          </Text>
        </View>
      </View>
  );
};

export default PokeCard;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: '97%',
    height: 120,
    margin: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: COLOR_SCHEME.PRIMARY,
  },
});

import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {SvgUri} from 'react-native-svg';
import IMAGE_ICONS from '../../assets';
import capitalizeFirstLetter from '../../helper/capitalizeFirstLetter';
import {COLOR_SCHEME} from '../ColorScheme/ColorScheme';

const CardList = ({number, url, name, onClick}) => {
  return (
    <TouchableOpacity onPress={() => onClick()}>
      <View style={styles.container}>
        <Text style={{height: '15%', fontSize: 10, marginRight: 5}}>
          #{number}
        </Text>
        <View
          style={{
            height: '70%',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
          }}>
          <SvgUri
            width="100%"
            height="100%"
            uri={`https://unpkg.com/pokeapi-sprites@2.0.2/sprites/pokemon/other/dream-world/${number}.svg`}
          />
        </View>
        <View
          style={{
            height: '15%',
            width: '100%',
            backgroundColor: COLOR_SCHEME.PRIMARY,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 12,
              fontWeight: '600',
              color: 'white',
            }}>
            {capitalizeFirstLetter(name)}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CardList;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: 105,
    height: 120,
    margin: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: COLOR_SCHEME.PRIMARY,
  },
});

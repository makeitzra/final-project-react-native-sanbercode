import { combineReducers } from '@reduxjs/toolkit'
import AuthReducer from './AuthReducer'
import PokeReducer from './PokeReducer'


const rootReducer = combineReducers({
    auth: AuthReducer,
    pokeball: PokeReducer
})


export default rootReducer
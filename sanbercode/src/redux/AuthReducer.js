import { createSlice } from "@reduxjs/toolkit";

const initialAuthState = {
    token: '',
    isLogin: false
};


const authSlice = createSlice({
  name: "authentication",
  initialState: initialAuthState,
  reducers: {
    login: (state, action) => {
      state.token = action.payload;
      state.isLogin = true;
    },
    logout: (state, action) => {
        state.token = '';
        state.isLogin = false
    }
  },
 
});

export const authActions = authSlice.actions;

export default authSlice.reducer;

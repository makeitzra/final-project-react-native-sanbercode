import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    pokeball: []
};


const pokeSlice = createSlice({
  name: "pokeball",
  initialState: initialState,
  reducers: {
    addPokemon: (state, action) => {
        state.pokeball = [...state.pokeball, action.payload]
    },
    removePokemon: (state, action) => {
        let filterPokeball = state.pokeball.filter((item) => item.name !== action.payload)
        state.pokeball = filterPokeball
    }
  },
 
});

export const pokeActions = pokeSlice.actions;

export default pokeSlice.reducer;

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { Image } from 'react-native';
import IMAGE_ICONS from '../assets';
import {COLOR_SCHEME} from '../components/ColorScheme/ColorScheme';
import Pokeball from '../pages/pokeball/Pokeball';
import Pokedex from '../pages/pokedex/Pokedex';

const Tab = createBottomTabNavigator();

const HomeRouteTab = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: COLOR_SCHEME.SECONDARY,
        tabBarInactiveTintColor: COLOR_SCHEME.PRIMARY,
      }}>
      <Tab.Screen
        name="Pokedex"
        component={Pokedex}
        options={{
          title: 'Pokedex',
          headerShown:false,
          tabBarIcon: ({ focused, color, size }) => (
            <Image source={IMAGE_ICONS.pokedex} style={{width: 25, height:25}}/>
          ),
        }}
      />
      <Tab.Screen
        name="Pokeball"
        component={Pokeball}
        options={{
          title: 'Pokeball',
          headerShown: false,
          tabBarIcon: ({ focused, color, size }) => (
            <Image source={IMAGE_ICONS.pokeball} style={{width: 25, height:25}}/>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default HomeRouteTab;
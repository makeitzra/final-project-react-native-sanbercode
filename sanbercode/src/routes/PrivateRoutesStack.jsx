import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {useSelector} from 'react-redux';
import LoginScreen from '../pages/login/LoginScreen';
import PokeDetail from '../pages/poke-detail/PokeDetail';
import HomeRouteTab from './HomeRouteTab';

const Stack = createNativeStackNavigator();

const PrivateRoutesStack = () => {
  const isLogin = useSelector(state => state.auth.isLogin);
  return (
    <Stack.Navigator initialRouteName="Login">
      {isLogin ? (
        <>
          <Stack.Screen
            name="HomeRouteTab"
            component={HomeRouteTab}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="PokemonDetail"
            component={PokeDetail}
            options={({ route }) => ({ title: route.params.name, headerTransparent: true, headerTintColor:'white' })}
          />
        </>
      ) : (
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
      )}
    </Stack.Navigator>
  );
};

export default PrivateRoutesStack;

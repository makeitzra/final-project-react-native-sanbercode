import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import IMAGE_ICONS from '../../assets';
import CardList from '../../components/CardList/CardList';
import {COLOR_SCHEME} from '../../components/ColorScheme/ColorScheme';
import capitalizeFirstLetter from '../../helper/capitalizeFirstLetter';
import splitNumber from '../../helper/splitNumber';
import {getListPoke, getPokeDetail} from '../../service/pokeAPI';

const Pokedex = ({route, navigation}) => {
  const [list, setList] = useState([]);
  const handleChange = e => {
    let filteredList = list.filter(item => item.name.includes(e));
    setList(filteredList);

    if (e.length === 0) {
      getList();
    }
  };

  const getList = async () => {
    try {
      const response = await getListPoke(null, null, null);
      if (response.ok) {
        setList(response.list);
      }
    } catch (err) {
      console.log(err);
      // Alert.alert(err);
    }
  };

  useEffect(() => {
    getList();
  }, []);

  const goToDetail = (url, name, number) => {
    navigation.push('PokemonDetail', {
      url: url,
      name: capitalizeFirstLetter(name),
      number: number
    });
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image style={styles.image} source={IMAGE_ICONS.pokeball} />
        <Text
          style={{
            fontSize: 25,
            color: COLOR_SCHEME.PRIMARY,
            fontWeight: '900',
            marginLeft: 10,
          }}>
          Pokédex
        </Text>
      </View>
      <View>
        <TextInput
          placeholder={'search'}
          style={styles.input}
          onChangeText={handleChange}
        />
      </View>
      {list.length > 0 ? (
        <View
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <FlatList
            data={list}
            numColumns={3}
            renderItem={item => (
              <CardList
                number={splitNumber(item.item.url)}
                name={item.item.name}
                onClick={() =>
                  goToDetail(item.item.url, item.item.name, item.index + 1)
                }
              />
            )}
          />
        </View>
      ) : (
        <ActivityIndicator />
      )}
    </View>
  );
};

export default Pokedex;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  image: {
    width: 20,
    height: 20,
  },
  input: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#ddd',
    padding: 5,
    borderRadius: 10,
    marginVertical: 10,
  },
});

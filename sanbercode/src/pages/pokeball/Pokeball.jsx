import React from 'react';
import {FlatList, Image, StyleSheet, Text, TextInput, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import IMAGE_ICONS from '../../assets';
import {COLOR_SCHEME} from '../../components/ColorScheme/ColorScheme';
import PokeCard from '../../components/PokeCard/PokeCard';
import { pokeActions } from '../../redux/PokeReducer';

const Pokeball = () => {

  const dispatch = useDispatch()  
  const listPokemon = useSelector(state => state.pokeball.pokeball);
  const handleChange = e => {
    let filteredList = list.filter(item => item.name.includes(e));
    setList(filteredList);

    if (e.length === 0) {
      getList();
    }
  };

  const removePokemon = (name) => {
    dispatch(pokeActions.removePokemon(name))
  }  

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image style={styles.image} source={IMAGE_ICONS.pokeball} />
        <Text
          style={{
            fontSize: 25,
            color: COLOR_SCHEME.PRIMARY,
            fontWeight: '900',
            marginLeft: 10,
          }}>
          Your Pokémon
        </Text>
      </View>
      <View
        style={{
          display: 'flex',
          flex: 1,
          flexDirection: 'row',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
          marginTop: 20
        }}>
        <FlatList
          data={listPokemon}
          numColumns={1}
          renderItem={item => (
            <PokeCard
              name={item.item.name}
              number={item.item.id}
              release={() => removePokemon(item.item.name)}
            />
          )}
        />
      </View>
    </View>
  );
};

export default Pokeball;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  image: {
    width: 20,
    height: 20,
  },
  input: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#ddd',
    padding: 5,
    borderRadius: 10,
    marginVertical: 10,
  },
});

import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  Image,
  ActivityIndicator,
  ProgressBarAndroidBase,
  TouchableOpacity,
} from 'react-native';
import {SvgUri} from 'react-native-svg';
import {useDispatch} from 'react-redux';
import IMAGE_ICONS from '../../assets';
import {colours} from '../../components/ColorScheme/PokeColor';
import capitalizeFirstLetter from '../../helper/capitalizeFirstLetter';
import {pokeActions} from '../../redux/PokeReducer';
import {getPokeDetail} from '../../service/pokeAPI';

const PokeDetail = ({navigation, route}) => {
  const [loading, setLoading] = useState(true);
  const [detail, setDetail] = useState({});
  const {url, name, number} = route.params;

  const dispatch = useDispatch();

  const getDetail = async () => {
    try {
      const response = await getPokeDetail(url);
      if (response.ok) {
        setDetail(response.detail);
        setLoading(false);
      }
    } catch (err) {
      console.log(err);
      // Alert.alert(err);
    }
  };

  useEffect(() => {
    setLoading(true);
    getDetail();
  }, []);

  const addPokemon = () => {
    dispatch(pokeActions.addPokemon(detail));
  };
  return (
    <View
      style={{
        height: '100%',
        backgroundColor: loading
          ? '#ddd'
          : colours[detail?.types[0]?.type?.name],
      }}>
      {loading ? (
        <ActivityIndicator />
      ) : (
        <>
          <View
            style={{width: '100%', display: 'flex', alignItems: 'flex-end'}}>
            <Image
              style={{width: 300, height: 300}}
              source={IMAGE_ICONS.pokeballBg}
            />
          </View>
          <View
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{marginTop: 100, marginBottom: -50, zIndex: 99}}>
              <SvgUri
                width={180}
                height={180}
                uri={`https://unpkg.com/pokeapi-sprites@2.0.2/sprites/pokemon/other/dream-world/${number}.svg`}
              />
            </View>
            <View
              style={{
                width: '90%',
                height: '60%',
                backgroundColor: 'white',
                borderRadius: 10,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginVertical: 10,
                  position: 'relative',
                  width: '100%',
                }}>
                <Text
                  style={{
                    backgroundColor: colours[detail?.types[0]?.type?.name],
                    color: 'white',
                    fontSize: 16,
                    padding: 5,
                    borderRadius: 10,
                  }}>
                  {capitalizeFirstLetter(detail?.types[0]?.type?.name)}
                </Text>

                <View
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: 50,
                    height: 50,
                    backgroundColor: colours[detail?.types[0]?.type?.name],
                    borderRadius: 40,
                    position: 'absolute',
                    right: 80,
                  }}>
                  <TouchableOpacity onPress={() => addPokemon()}>
                    <Image
                      style={{width: 40, height: 40}}
                      source={IMAGE_ICONS.pokeballWhite}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <Text
                style={{
                  marginVertical: 15,
                  color: colours[detail?.types[0]?.type?.name],
                  fontWeight: '600',
                  fontSize: 18,
                }}>
                About
              </Text>
              <View style={{marginVertical: 10, flexDirection: 'row'}}>
                <View
                  style={{
                    width: 90,
                    height: 70,
                    borderRightColor: '#ddd',
                    borderRightWidth: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{marginVertical: 10, fontWeight: '900'}}>
                    {detail.weight} kg
                  </Text>
                  <Text>Weight</Text>
                </View>
                <View
                  style={{
                    width: 90,
                    height: 70,
                    borderRightColor: '#ddd',
                    borderRightWidth: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{marginVertical: 10, fontWeight: '900'}}>
                    {detail.height} m
                  </Text>
                  <Text>Height</Text>
                </View>
                <View
                  style={{
                    width: 90,
                    height: 70,
                    borderRightColor: '#ddd',
                    borderRightWidth: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{marginVertical: 10, fontWeight: '900'}}>
                    {detail.moves[0].move.name}
                  </Text>
                  <Text>Move</Text>
                </View>
              </View>
              <View
                style={{
                  marginVertical: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: colours[detail?.types[0]?.type?.name],
                    fontWeight: '600',
                    fontSize: 18,
                    marginBottom: 10,
                  }}>
                  Base Stat
                </Text>
                <View
                  style={{
                    display: 'flex',
                    width: '100%',
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    paddingHorizontal: 20,
                  }}>
                  {detail.stats.slice(0, 3).map(item => {
                    return (
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'center',
                          width: '100%',
                        }}>
                        <Text style={{width: '30%', textAlign: 'left'}}>
                          {item.stat.name}
                        </Text>
                        <Text style={{width: '10%'}}>{item.base_stat}</Text>
                        <View style={{width: '50%'}}>
                          <View
                            style={{
                              width: `${item.base_stat}%`,
                              height: 5,
                              backgroundColor:
                                colours[detail?.types[0]?.type?.name],
                              borderRadius: 5,
                            }}></View>
                        </View>
                      </View>
                    );
                  })}
                </View>
              </View>
            </View>
          </View>
        </>
      )}
    </View>
  );
};

export default PokeDetail;

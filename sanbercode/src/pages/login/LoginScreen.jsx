import axios from 'axios';
import QueryString from 'qs';
import {useEffect, useState} from 'react';
import {Dimensions, Image, StyleSheet, Text, View} from 'react-native';
import {useDispatch} from 'react-redux';
import IMAGE_ICONS from '../../assets';
import CustomButton from '../../components/Button/CustomButton';
import {COLOR_SCHEME} from '../../components/ColorScheme/ColorScheme';
import TextFields from '../../components/TextFields/TextFields';
import {authActions} from '../../redux/AuthReducer';

const LoginScreen = ({navigation}) => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });
  const [name, setName] = useState('');

  const dispatch = useDispatch();

  const handleChange = (e, name) => {
    setFormData({
      ...formData,
      [name]: e,
    });
  };

  const handleLogin = async () => {
    try {
      const options = {
        method: 'POST',
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        data: QueryString.stringify(formData),
        url: 'https://demoapi-hilmy.sanbercloud.com/api/login',
      };
      const result = await axios(options);
      if (result.status === 200) {
        dispatch(authActions.login(result.data.token));
      }
    } catch (err) {
      console.log(err, 'error');
    }
  };

  return (
    <View style={styles.container}>
      <View
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image style={styles.image} source={IMAGE_ICONS.pokeball} />
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.titleText}>Pokédex</Text>
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.titleLogin}>Login</Text>
      </View>
      <View style={styles.formContainer}>
        <TextFields
          placeholder={'Please enter your email'}
          label={'Email'}
          onChange={e => handleChange(e, 'email')}
        />
        <TextFields
          placeholder={'Please enter your password'}
          label={'Password'}
          onChange={e => handleChange(e, 'password')}
        />
      </View>
      <View style={styles.buttonContainer}>
        <CustomButton variant={'primary'} onPress={() => handleLogin()}>
          Submit
        </CustomButton>
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  titleContainer: {
    width: Dimensions.get('window').width,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    marginTop: -20,
    fontSize: 30,
    fontWeight: '900',
    color: COLOR_SCHEME.PRIMARY,
  },
  titleLogin: {
    marginTop: 50,
    marginBottom: -20,
    fontSize: 25,
    fontWeight: '900',
    color: COLOR_SCHEME.PRIMARY,
  },
  formContainer: {
    width: Dimensions.get('window').width,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    marginTop: 50,
  },
  buttonContainer: {
    width: Dimensions.get('window').width,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    marginTop: 10,
  },
  image: {
    width: 200,
    resizeMode: 'contain',
  },
});

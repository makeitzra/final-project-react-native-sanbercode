/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import {NavigationContainer} from '@react-navigation/native';
import React, { useEffect } from 'react';
import {
  SafeAreaView,
} from 'react-native';

import {Provider} from 'react-redux';
import store from './src/redux/store';
import PrivateRoutesStack from './src/routes/PrivateRoutesStack';
import SplashScreen from 'react-native-splash-screen'

const App = () => {

  useEffect(() => {
    SplashScreen.hide()
  }, [])

  return (
    <Provider store={store}>
      <SafeAreaView style={{flex: 1}}>
        <NavigationContainer>
          <PrivateRoutesStack />
        </NavigationContainer>
      </SafeAreaView>
    </Provider>
  );
};

export default App;
